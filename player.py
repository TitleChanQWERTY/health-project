import pygame
import math
from config import createImage, WINDOW_SIZE
from BULLET import bullet_player
from sfx import powerUp, shoot


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/PLAYER/f_player.png")
        self.image = pygame.transform.scale(self.image, (28, 28))
        self.rect = self.image.get_rect(center=(x, y))

        self.Life: int = 5
        self.exp: int = 0
        self.speedMove: int = 9

        self.timeShoot: int = 0
        self.timeShootMax: int = 7

        self.damage: int = 5

        self.shootPos: int = 11

    def update(self):
        # Create Key Detect
        key = pygame.key.get_pressed()

        # Slow Move
        if key[pygame.K_LSHIFT]:
            self.shootPos = 2
            self.speedMove = 4
        else:
            self.shootPos = 11
            self.speedMove = 9

        # Shoot Man!
        if key[pygame.K_c]:
            self.shoot_bullet()

        # Start Move Func
        self.move(key)

    def shoot_bullet(self):
        if self.timeShoot >= self.timeShootMax:
            bullet_player.createBulletStandart(self.rect.centerx, self.rect.centery)
            if self.exp >= 90:
                bullet_player.createBulletPistol(self.rect.centerx - self.shootPos, self.rect.centery - 15)
                bullet_player.createBulletPistol(self.rect.centerx + self.shootPos + 5, self.rect.centery - 15)
            shoot.play()
            self.timeShoot = 0
        self.timeShoot += 1

    def move(self, key):
        movement_direction_x = key[pygame.K_RIGHT] - key[pygame.K_LEFT]
        movement_direction_y = key[pygame.K_DOWN] - key[pygame.K_UP]

        movement_length = math.sqrt(movement_direction_x ** 2 + movement_direction_y ** 2)
        if movement_length <= 0:
            return
        
        movement_direction_x /= movement_length
        movement_direction_y /= movement_length
        
        self.rect.y += round(movement_direction_y * self.speedMove)
        self.rect.x += round(movement_direction_x * self.speedMove)

        self.rect.x = max(WINDOW_SIZE[0]//2+5, min(1130, self.rect.x))
        self.rect.y = max(5, min(685, self.rect.y))

    def check_exp(self):
        if self.exp == 50 or self.exp == 90:
            powerUp.play()
            self.timeShootMax = 4
        if self.exp >= 30:
            self.damage = self.exp // 35 + 6
