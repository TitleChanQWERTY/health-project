from random import randint

import pygame

from config import createImage, WINDOW_SIZE
from group_config import particle


class Particle(pygame.sprite.Sprite):
    def __init__(self, x, y, size, filename):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(particle)

        self.MaxTime: int = randint(100, 435)
        self.time: int = 0
        self.radius = size[0] - 6

        self.size = size
        self.filename = filename

        self.posMoveX = randint(-10, 35) / 9 - 1
        self.posY = randint(0, 1)

        if self.posY == 1:
            self.posMoveY = randint(0, 6)
        else:
            self.posMoveY = randint(-6, 0)

        self.x, self.y = x, y

        self.angle: int = 0
        self.angleRot = randint(-10, 45) / 5

    def update(self):
        self.y += self.posMoveY + 1
        self.x += self.posMoveX
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, self.size)
        self.angle += self.angleRot
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.image.set_alpha((1 - self.time / self.MaxTime) * 255)
        self.rect = self.image.get_rect(center=(self.x, self.y))
        self.time += 1
        if self.time >= self.MaxTime or self.rect.y > 725 or self.rect.y < 0 or self.rect.x < WINDOW_SIZE[0]//2+36 \
                or self.rect.x > 1150:
            self.kill()


def particleSystem(x, y, size, count, filename):
    for i in range(count):
        Particle(x, y, size, filename)
