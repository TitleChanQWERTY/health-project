import pygame
from config import createImage
from group_config import bullet_player


class BULLET_STANDART(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1.png")
        self.image = pygame.transform.scale(self.image, (12, 12))
        self.x, self.y = x, y
        self.rect = self.image.get_rect(center=(self.x, self.y))

        self.angle = 0

        self.add(bullet_player)

    def update(self):
        if self.y < 8:
            self.kill()

        self.y -= 21

        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/1.png")
        self.image = pygame.transform.scale(self.image, (12, 12))
        self.angle += 1
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect(center=(self.x, self.y))


class BULLET_PISTOL(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BULLET/PLAYER/2.png")
        self.image = pygame.transform.scale(self.image, (12, 12))
        self.rect = self.image.get_rect(center=(x, y))

        self.speedFly: int = 0

        self.add(bullet_player)

    def update(self):
        if self.rect.y < 8:
            self.kill()

        self.rect.y -= self.speedFly
        self.speedFly += 0.5


def createBulletStandart(x, y):
    BULLET_STANDART(x, y)


def createBulletPistol(x, y):
    BULLET_PISTOL(x, y)
