from random import randint

import pygame
from group_config import bullet_enemy
from config import createImage, WINDOW_SIZE


class Simple(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size=(14, 15)):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(bullet_enemy)

    def update(self):
        if self.rect.y > 690:
            self.kill()
        self.rect.y += 7


class AnyPosBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(bullet_enemy)

        self.xSpeed: int = randint(-2, 2)
        self.ySpeed: int = randint(2, 5)

    def update(self):
        if self.rect.y > 725 or self.rect.x <= WINDOW_SIZE[0] // 2 + 20 or self.rect.x > 1125:
            self.kill()
        self.rect.y += self.ySpeed
        self.rect.x += self.xSpeed


class bulletFixPos(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, pos):
        super().__init__()
        self.image = createImage(filename)
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(bullet_enemy)

        self.pos = pos

    def update(self):
        if self.rect.y > 725 or self.rect.x <= WINDOW_SIZE[0] // 2 + 20 or self.rect.x > 1125:
            self.kill()
        if self.pos == 0:
            self.rect.y += 4
        if self.pos == 1:
            self.rect.y += 4
            self.rect.x -= 4
        if self.pos == 2:
            self.rect.x -= 4
        if self.pos == 3:
            self.rect.y -= 4
            self.rect.x -= 4
        if self.pos == 4:
            self.rect.y -= 4
        if self.pos == 5:
            self.rect.x += 4
            self.rect.y -= 4
        if self.pos == 6:
            self.rect.x += 4
        if self.pos == 7:
            self.rect.y += 4
            self.rect.x += 4
