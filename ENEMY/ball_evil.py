from random import randint

import pygame
import math
from config import createImage
from group_config import enemy
from BULLET import bullet_enemy
from ITEM import exp
from particle import particleSystem
from sfx import die_enemy
from config import WINDOW_SIZE


class Evil(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_EVIL/1.png")
        self.rect = self.image.get_rect(center=(x, y))

        self.Health: int = 10
        self.speed: int = randint(2, 4)
        self.spawn_time: int = pygame.time.get_ticks()

        self.Frame: int = 0

        self.add(enemy)

        self.Lucky: int = randint(0, 1)

        self.isTakeDamage: int = False
        self.timeDamage: int = 0

    def update(self):
        if self.rect.y > WINDOW_SIZE[1] or self.Health <= 0:
            if self.Health <= 0:
                if self.Lucky == 1:
                    exp.Exp(self.rect.centerx, self.rect.centery)
                particleSystem(self.rect.centerx, self.rect.centery, (14, 14), 3, "ASSETS/SPRITE/EFFECT/2.png")
                die_enemy.play()
            self.kill()
        
        self.move()

        if not self.isTakeDamage:
            if self.Frame == 15:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_EVIL/1.png")
                self.image = pygame.transform.scale(self.image, (27, 27))
            if self.Frame == 55:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_EVIL/2.png")
                self.image = pygame.transform.scale(self.image, (27, 27))
                bullet_enemy.Simple(self.rect.centerx+5, self.rect.centery+6, "ASSETS/SPRITE/BULLET/ENEMY/1.png")
                self.Frame = 0
            self.Frame += 1
        else:
            if self.timeDamage >= 1:
                self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_EVIL/1.png")
                self.image = pygame.transform.scale(self.image, (27, 27))
                self.Frame = 15
                self.isTakeDamage = False
            self.image = createImage("ASSETS/SPRITE/ENEMY/BALL_EVIL/damage.png")
            self.image = pygame.transform.scale(self.image, (27, 27))
            self.timeDamage += 0.6
        
    def move(self):
        change_direction_time = 1250
        time = (self.spawn_time + pygame.time.get_ticks()) / change_direction_time * math.pi
        movement_direction_x = round(math.copysign(1, math.sin(time)))
        movement_direction_y = 1
        
        self.rect.x += round(movement_direction_x)
        self.rect.y += round(movement_direction_y * self.speed)

        self.rect.x = max(WINDOW_SIZE[0]//2+5, min(1130, self.rect.x))
