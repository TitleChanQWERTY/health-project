import math

import pygame
from config import createImage, WINDOW_SIZE, sc
from group_config import enemy
from BULLET import bullet_enemy


class Boss(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/BOSS/FUCK_COVID/1.png")
        self.image = pygame.transform.scale(self.image, (47, 94))
        self.rect = self.image.get_rect(center=(WINDOW_SIZE[0] // 2 + 315, 115))
        self.add(enemy)

        self.Health: int = 19990
        self.spawn_time: int = pygame.time.get_ticks()

        self.phase: int = 0

        self.timeShoot: int = 0
        self.timeShootExtra: int = 0
        self.countBullet: int = 0

        self.startShoot: int = 0

        self.Frame: int = 0

        self.fontHealth = pygame.font.Font("ASSETS/FONT/joystix monospace.otf", 19)

    def check_health(self):
        if self.Health <= 16450:
            self.phase = 1
        if self.Health <= 10655:
            self.phase = 2
        if self.Health <= 5595:
            self.phase = 3

    def bulletOne(self):
        if self.timeShoot >= 55:
            for i in range(14):
                bullet_enemy.AnyPosBullet(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/6.png",
                                          (34, 34))
            self.timeShoot = 0
        self.timeShoot += 1

    def bulletTwo(self):
        if self.timeShoot >= 6:
            if self.countBullet < 8:
                bullet_enemy.bulletFixPos(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/7.png",
                                          (59, 59), self.countBullet)
                self.countBullet += 1
                self.timeShoot = 0
            else:
                self.countBullet = 0
        self.timeShoot += 1

    def bulletThree(self):
        if self.timeShootExtra >= 50:
            for i in range(5):
                bullet_enemy.AnyPosBullet(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/8.png",
                                          (34, 33))
            self.timeShootExtra = 0
        self.timeShootExtra += 1
        if self.timeShoot >= 7:
            if self.countBullet < 8:
                bullet_enemy.bulletFixPos(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/7.png",
                                          (39, 39), self.countBullet)
                self.countBullet += 1
                self.timeShoot = 0
            else:
                self.countBullet = 0
        self.timeShoot += 1

    def bulletFour(self):
        if self.timeShootExtra >= 115:
            if self.countBullet <= 20:
                for i in range(5):
                    bullet_enemy.AnyPosBullet(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/8.png",
                                              (34, 33))
                self.countBullet += 1
            else:
                self.timeShootExtra = 0
        else:
            self.timeShootExtra += 1
            self.countBullet = 0

    def bulletShoot(self):
        if self.phase == 0:
            self.bulletOne()
        if self.phase == 1:
            self.bulletTwo()
        if self.phase == 2:
            self.bulletThree()
        if self.phase == 3:
            self.bulletFour()

    def anim(self):
        if self.Frame == 20:
            self.Frame += 1
            self.image = createImage("ASSETS/SPRITE/BOSS/FUCK_COVID/1.png")
            self.image = pygame.transform.scale(self.image, (47, 94))
        if self.Frame == 40:
            self.Frame = 0
            self.image = createImage("ASSETS/SPRITE/BOSS/FUCK_COVID/2.png")
            self.image = pygame.transform.scale(self.image, (47, 94))
        self.Frame += 1

    def update(self):
        self.anim()
        if self.startShoot >= 150:
            txtHealth = self.fontHealth.render(str(self.Health), False, (255, 0, 0))
            sc.blit(txtHealth, txtHealth.get_rect(center=(self.rect.centerx, self.rect.centery - 65)))
            if self.phase == 0 or self.phase >= 2:
                change_direction_time = 5505
                time = (self.spawn_time + pygame.time.get_ticks()) / change_direction_time * math.pi
                movement_direction_x = round(math.copysign(1, math.sin(time)))

                self.rect.x += round(movement_direction_x)

                self.rect.x = max(WINDOW_SIZE[0] // 2 + 5, min(1120, self.rect.x))
            self.check_health()
            self.bulletShoot()
        else:
            self.Health = 19990
            self.startShoot += 1
