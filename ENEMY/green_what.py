from random import randint

import pygame
from config import createImage, WINDOW_SIZE
from BULLET import bullet_enemy
from group_config import enemy
from particle import particleSystem
from sfx import die_enemy_hard


class Green(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/GREEN_WHAT/1.png")
        self.image = pygame.transform.scale(self.image, (38, 38))
        self.image = pygame.transform.flip(self.image, False, True)
        self.rect = self.image.get_rect(center=(randint(WINDOW_SIZE[0] // 2 + 15, 1130), 790))

        self.add(enemy)

        self.Health = 110

        self.timeShoot = 0

        self.isTakeDamage: int = False
        self.timeDamage: int = 0

    def shoot(self):
        if self.timeShoot >= 140:
            for i in range(4):
                bullet_enemy.AnyPosBullet(self.rect.centerx, self.rect.centery-10, "ASSETS/SPRITE/BULLET/ENEMY/2.png",
                                          (17, 17))
            self.timeShoot = 0
        self.timeShoot += 1

    def update(self):
        self.shoot()
        if self.rect.y < 0 or self.Health <= 0:
            if self.Health <= 0:
                die_enemy_hard.play()
                particleSystem(self.rect.centerx, self.rect.centery, (18, 18), 4, "ASSETS/SPRITE/EFFECT/3.png")
            self.kill()

        self.rect.y -= 2

        if self.isTakeDamage:
            if self.timeDamage >= 0.5:
                self.image = createImage("ASSETS/SPRITE/ENEMY/GREEN_WHAT/1.png")
                self.image = pygame.transform.scale(self.image, (38, 38))
                self.image = pygame.transform.flip(self.image, False, True)
                self.isTakeDamage = False
            self.image = createImage("ASSETS/SPRITE/ENEMY/GREEN_WHAT/damage.png")
            self.image = pygame.transform.scale(self.image, (38, 38))
            self.image = pygame.transform.flip(self.image, False, True)
            self.timeDamage += 1
        else:
            self.image = createImage("ASSETS/SPRITE/ENEMY/GREEN_WHAT/1.png")
            self.image = pygame.transform.scale(self.image, (38, 38))
            self.image = pygame.transform.flip(self.image, False, True)
