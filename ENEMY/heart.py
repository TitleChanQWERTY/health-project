from random import randint

import pygame
from config import createImage, WINDOW_SIZE
from particle import particleSystem
from sfx import die_enemy_hard
from group_config import enemy
from BULLET import bullet_enemy
from ITEM import life


class Heart(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ENEMY/HEART/1.png")
        self.image = pygame.transform.scale(self.image, (30, 30))
        self.rect = self.image.get_rect(center=(randint(WINDOW_SIZE[0] // 2 + 15, 1130), -55))

        self.add(enemy)

        self.Health: int = 150

        self.timeShoot: int = 0

        self.isTakeDamage: int = False
        self.timeDamage: int = 0

    def shoot(self):
        if self.timeShoot >= 75:
            bullet_enemy.Simple(self.rect.centerx - 15, self.rect.centery-1, "ASSETS/SPRITE/BULLET/ENEMY/4.png", (11, 11))
            bullet_enemy.Simple(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/5.png", (12, 12))
            bullet_enemy.Simple(self.rect.centerx + 15, self.rect.centery-1, "ASSETS/SPRITE/BULLET/ENEMY/4.png", (11, 11))
            self.timeShoot = 0
        self.timeShoot += 1

    def update(self):
        self.shoot()
        if self.rect.y > 750 or self.Health <= 0:
            if self.Health <= 0:
                life.Life(self.rect.centerx, self.rect.centery)
                particleSystem(self.rect.centerx, self.rect.centery, (15, 15), randint(3, 6),
                               "ASSETS/SPRITE/EFFECT/2.png")
                die_enemy_hard.play()
            self.kill()

        self.rect.y += 4

        if self.isTakeDamage:
            if self.timeDamage >= 1.5:
                self.isTakeDamage = False
            self.image = createImage("ASSETS/SPRITE/ENEMY/HEART/damage.png")
            self.image = pygame.transform.scale(self.image, (30, 30))
            self.timeDamage += 1
        else:
            self.image = createImage("ASSETS/SPRITE/ENEMY/HEART/1.png")
            self.image = pygame.transform.scale(self.image, (30, 30))
