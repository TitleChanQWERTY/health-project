from random import randint

import pygame

from ITEM import exp
from config import createImage
from group_config import enemy
from BULLET import bullet_enemy
from particle import particleSystem
from sfx import die_enemy
from config import WINDOW_SIZE


class CovidLike(pygame.sprite.Sprite):
    def __init__(self, x, filename, size):
        super().__init__()
        self.filename = filename
        self.size = size
        self.image = createImage(self.filename)
        self.image = pygame.transform.scale(self.image, size)
        self.x, self.y = x, -65
        self.rect = self.image.get_rect(center=(self.x, self.y))

        self.add(enemy)

        self.Health: int = 35

        self.attackTime: int = 0
        self.attackCooldown: int = 125

        self.fall_speed: int = 1
        target_height_range: int = 100
        target_height_difference: int = randint(-target_height_range, target_height_range)
        self.target_height: int = WINDOW_SIZE[1] // 2 + target_height_difference

        self.angle = 0
        self.angleSpeed = randint(1, 5)

        self.isTakeDamage: int = False
        self.timeDamage: int = 0

        self.luck_item: int = randint(0, 1)

    def shoot(self):
        if self.attackTime <= 0:
            for i in range(4):
                bullet_enemy.AnyPosBullet(self.rect.centerx, self.rect.centery, "ASSETS/SPRITE/BULLET/ENEMY/3.png", (12, 12))
            self.attackTime = self.attackCooldown
        self.attackTime -= 1

    def update(self):
        self.shoot()
        if self.Health <= 0:
            if self.luck_item == 1:
                exp.Exp(self.rect.centerx, self.rect.centery)
            particleSystem(self.rect.centerx, self.rect.centery, (14, 14), 3, "ASSETS/SPRITE/EFFECT/3.png")
            die_enemy.play()
            self.kill()
        self.move()
        self.draw()

    def move(self):
        if self.y < self.target_height:
            self.y += self.fall_speed
    
    def draw(self):
        if not self.isTakeDamage:
            self.image = createImage(self.filename)
            self.image = pygame.transform.scale(self.image, self.size)
            self.angle += self.angleSpeed
            self.image = pygame.transform.rotate(self.image, self.angle)
        else:
            if self.timeDamage >= 1.5:
                self.image = createImage(self.filename)
                self.image = pygame.transform.scale(self.image, self.size)
                self.isTakeDamage = False
            self.image = createImage("ASSETS/SPRITE/ENEMY/COVID_LIKE/damage.png")
            self.image = pygame.transform.scale(self.image, self.size)
            self.timeDamage += 1
        self.rect = self.image.get_rect(center=(self.x, self.y))
