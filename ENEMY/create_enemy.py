from random import randint

from ENEMY import ball_evil, covid_like, green_what, heart
from config import WINDOW_SIZE

timeCreateBall: int = 0
timeCreateCovidLike: int = 0
timeCreateGreenWhat: int = 0
timeCreateHeart: int = 0


def stageCreateEnemy():
    global timeCreateBall, timeCreateCovidLike, timeCreateGreenWhat, timeCreateHeart
    if timeCreateBall > 125:
        ball_evil.Evil(randint(WINDOW_SIZE[0] // 2 + 15, 1130), -57)
        timeCreateBall = 0
    if timeCreateCovidLike > 645:
        covid_like.CovidLike(randint(WINDOW_SIZE[0] // 2 + 15, 1130), "ASSETS/SPRITE/ENEMY/COVID_LIKE/1.png", (26, 26))
        timeCreateCovidLike = randint(300, 450)
    if timeCreateGreenWhat >= 1265:
        green_what.Green()
        timeCreateGreenWhat = 890
    if timeCreateHeart >= 1650:
        heart.Heart()
        timeCreateHeart = 950

    timeCreateHeart += 1
    timeCreateGreenWhat += 1
    timeCreateBall += 1
    timeCreateCovidLike += 1
