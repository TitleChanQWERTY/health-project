from pygame import mixer

sfx_volume = 1

pickup = mixer.Sound("ASSETS/SFX/pickupCoin.wav")
powerUp = mixer.Sound("ASSETS/SFX/powerUp.wav")
die_enemy = mixer.Sound("ASSETS/SFX/explosion.wav")
die_enemy_hard = mixer.Sound("ASSETS/SFX/explosion(1).wav")
shoot = mixer.Sound("ASSETS/SFX/laserShoot.wav")
shoot_volume = 0.11
hit = mixer.Sound("ASSETS/SFX/hitHurt.wav")

sounds = [pickup, powerUp, die_enemy, die_enemy_hard, shoot, hit]

def update_volume(vol):
    global sfx_volume
    sfx_volume = max(0, min(1, vol))
    for sound in sounds:
        sound.set_volume(sfx_volume)
        if sound == shoot:
            sound.set_volume(sfx_volume * shoot_volume)

update_volume(sfx_volume)