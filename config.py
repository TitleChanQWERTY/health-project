import pygame

pygame.init()

WINDOW_SIZE = (1200, 720)

sc = pygame.display.set_mode(WINDOW_SIZE)
clock = pygame.time.Clock()

surf_game_zone = pygame.surface.Surface((565, 720))
surf_game_zone.fill((222, 189, 230))

select_scene = None


def switch_scene(scene):
    global select_scene
    select_scene = scene


def createImage(filename):
    return pygame.image.load(filename).convert_alpha()
