import pygame
from font_config import ScoreFont
from group_config import floating_text
from config import sc


class FloatingText(pygame.sprite.Sprite):
    def __init__(self, x, y, text, color):
        super().__init__()
        font = pygame.font.Font("ASSETS/FONT/dpcomic.ttf", 22)
        self.text_surface = font.render(text, False, color)
        self.rect = self.text_surface.get_rect(center=(x, y))
        self.image = self.text_surface
        self.lifetime = 100
        self.add(floating_text)

        self.alpha: int = 250

    def update(self):
        self.alpha -= 4
        self.text_surface.set_alpha(self.alpha)
        self.rect.y -= 1
        self.lifetime -= 1
        if self.lifetime <= 0:
            self.kill()
