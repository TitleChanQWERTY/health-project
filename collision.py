from random import randint
from pygame import transform

import group_config
from config import WINDOW_SIZE, createImage
from SCENE import scene_game
from sfx import pickup, hit
from floating_text import FloatingText

timeDamage: int = 150
timeAlpha: int = 0


def collision(p):
    global timeDamage, timeAlpha
    if timeDamage >= 140:
        p.image = createImage("ASSETS/SPRITE/PLAYER/f_player.png")
        p.image = transform.scale(p.image, (28, 28))
        for b in group_config.bullet_enemy:
            if b.rect.collidepoint(p.rect.center):
                p.Life -= 1
                hit.play()
                p.rect.x = WINDOW_SIZE[0] // 2 + 315
                p.rect.y = WINDOW_SIZE[1] // 2 + 310
                timeDamage = 0
                b.kill()
    else:
        for b in group_config.bullet_enemy:
            b.kill()
        p.image = createImage("ASSETS/SPRITE/ENEMY/HEART/damage.png")
        p.image = transform.scale(p.image, (28, 28))
        p.image = transform.flip(p.image, False, True)
        if timeAlpha < 15:
            p.image.set_alpha(130)
        else:
            timeAlpha = 0
            p.image.set_alpha(300)
        timeAlpha += 1
        timeDamage += 1

    for b_p in group_config.bullet_player:
        for e in group_config.enemy:
            if e.rect.colliderect(b_p.rect):
                e.isTakeDamage = True
                e.Health -= p.damage
                scene_game.SCORE += randint(1, 5)
                b_p.kill()

    for i in group_config.item:
        if p.rect.colliderect(i.rect):
            if i.id == 0:
                p.exp += 10
                FloatingText(i.rect.centerx, i.rect.centery, "+10", (250, 250, 0))
                p.check_exp()
                pickup.play()
                i.kill()
            else:
                p.Life += 1
                FloatingText(i.rect.centerx, i.rect.centery, "+1", (252, 50, 246))
                pickup.play()
                i.kill()
