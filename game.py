import pygame
import config
from SCENE import scene_menu

pygame.display.set_caption("HEALTH::PROJECT")
pygame.display.set_icon(config.createImage("ASSETS/SPRITE/ICO/logoheart.png"))


config.switch_scene(scene_menu.scene)
while config.select_scene is not None:
    var = config.select_scene()
