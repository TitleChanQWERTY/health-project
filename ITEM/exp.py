from random import randint

import pygame
from config import createImage
from group_config import item


class Exp(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/POWER_UP.png")
        self.image = pygame.transform.scale(self.image, (23, 23))
        self.x, self.y = x, y
        self.rect = self.image.get_rect(center=(self.x, self.y))

        self.id: int = 0

        self.add(item)

        self.angle = 0

        self.downMax: int = randint(2, 3)
        self.upMax: int = 5

    def update(self):
        if self.y > 725:
            self.kill()

        if self.upMax <= 0:
            self.y += self.downMax
        else:
            self.y -= self.upMax
            self.upMax -= 0.5

        self.image = createImage("ASSETS/SPRITE/ITEM/POWER_UP.png")
        self.image = pygame.transform.scale(self.image, (23, 23))
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.angle += 3
        self.rect = self.image.get_rect(center=(self.x, self.y))
