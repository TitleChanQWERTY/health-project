import pygame
from config import createImage
from group_config import item


class Life(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/ITEM/HEALTH.png")
        self.image = pygame.transform.scale(self.image, (29, 29))
        self.rect = self.image.get_rect(center=(x, y))

        self.id: int = 1

        self.add(item)

    def update(self):
        if self.rect.y > 750:
            self.kill()
        self.rect.y += 3
