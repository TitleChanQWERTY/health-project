from random import randint

import pygame
from group_config import particle
from config import createImage, WINDOW_SIZE


class Back(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = createImage("ASSETS/SPRITE/EFFECT/4.png")
        self.image = pygame.transform.scale(self.image, (11, 11))
        self.rect = self.image.get_rect(center=(randint(WINDOW_SIZE[0]//2+15, 1125), randint(-95, 700)))
        self.image.set_alpha(randint(20, 190))
        self.speedMove: int = randint(1, 4)
        self.add(particle)

    def update(self):
        self.rect.y += self.speedMove
        if self.rect.y >= 740:
            self.rect.y = randint(-125, -50)
            self.rect.x = randint(WINDOW_SIZE[0]//2+15, 1125)
