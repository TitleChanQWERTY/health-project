import pygame
import back_effect
import sfx
from enum import Enum
from config import sc, clock, WINDOW_SIZE, switch_scene, surf_game_zone, createImage
from SCENE import scene_die, scene_menu, scene_boss
from ENEMY import create_enemy
from font_config import ScoreFont, LifeFont, ExpFont
from collision import collision
from player import Player
from screenshot import take
import group_config

SCORE: int = 0


class PauseOptions(Enum):
    CONTINUE = 0
    RESTART = 1
    QUIT = 2
    MUSICVOLUME = 3
    SFXVOLUME = 4


gray_option_alpha = 30
full_option_alpha = 300

ScoreTXT = ScoreFont.render("SCORE -> " + str(SCORE), False, (255, 255, 255))


def updateTXT():
    global ScoreTXT
    ScoreTXT = ScoreFont.render("SCORE -> " + str(SCORE), False, (255, 255, 255))


def draw_group():
    group_config.particle.draw(sc)
    group_config.bullet_player.draw(sc)
    group_config.enemy.draw(sc)
    group_config.bullet_enemy.draw(sc)
    group_config.item.draw(sc)
    group_config.floating_text.draw(sc)


def update_group():
    group_config.bullet_player.update()
    group_config.enemy.update()
    group_config.bullet_enemy.update()
    group_config.item.update()
    group_config.particle.update()
    group_config.floating_text.update()


player = Player(WINDOW_SIZE[0] // 2 + 315, WINDOW_SIZE[1] // 2 + 300)


def restart_scene():
    global player
    player = Player(WINDOW_SIZE[0] // 2 + 315, WINDOW_SIZE[1] // 2 + 300)
    create_enemy.timeCreateBall = 0
    create_enemy.timeCreateCovidLike = 0
    create_enemy.timeCreateGreenWhat = 0
    create_enemy.timeCreateHeart = 0
    for e in group_config.enemy:
        e.kill()
    for i in group_config.item:
        i.kill()
    for p in group_config.particle:
        p.kill()
    for b_p in group_config.bullet_player:
        b_p.kill()
    for e_b in group_config.bullet_enemy:
        e_b.kill()


def scene():
    restart_scene()

    for i in range(17):
        back_effect.Back()

    running: bool = True
    FPS: int = 60

    LOGO = createImage("ASSETS/SPRITE/UI/LOGO.png")
    LOGO = pygame.transform.scale(LOGO, (200, 200))

    pygame.mixer.music.load("ASSETS/OST/stage.mp3")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.80)

    isPause: bool = False

    PauseTXT = ScoreFont.render("YEA PAUSE", False, (255, 255, 255))

    BackTXT = ExpFont.render("CONTINUE", False, (255, 255, 255))
    RestartTXT = ExpFont.render("RESTART", False, (255, 255, 255))
    QuitTXT = ExpFont.render("QUIT", False, (255, 255, 255))
    MusicVolumeText = ExpFont.render("Music Volume: %.2f" % pygame.mixer.music.get_volume(), False, (255, 255, 255))
    SFXVolumeText = ExpFont.render("SFX Volume: %.2f" % sfx.sfx_volume, False, (255, 255, 255))

    ArrowImg = createImage("ASSETS/SPRITE/UI/ARROW.png")
    ArrowImg = pygame.transform.scale(ArrowImg, (35, 25))
    YArrow = WINDOW_SIZE[1] // 2 - 150

    pause_texts = [BackTXT, RestartTXT, QuitTXT, MusicVolumeText, SFXVolumeText]

    ImageFill = createImage("ASSETS/SPRITE/UI/FILL_IMAGE/1.png")
    ImageFill = pygame.transform.scale(ImageFill, WINDOW_SIZE)
    ImageFill.set_alpha(175)

    select_pause: int = 0

    alphaImg: int = 0
    timeEnd: int = 0
    timeStartBoss: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take()
                if e.key == pygame.K_ESCAPE:
                    if isPause:
                        isPause = False
                    else:
                        isPause = True
                if isPause:
                    if e.key == pygame.K_DOWN:
                        select_pause += 1

                    if e.key == pygame.K_UP:
                        select_pause -= 1

                    select_pause %= len(PauseOptions)

                    if e.key == pygame.K_LEFT:
                        if select_pause == PauseOptions.MUSICVOLUME.value:
                            current_volume = pygame.mixer.music.get_volume()
                            new_volume = max(0, min(1, current_volume - 0.1))
                            pygame.mixer.music.set_volume(new_volume)
                            MusicVolumeText = ExpFont.render("Music Volume: %.2f" % new_volume, False, (255, 255, 255))
                        if select_pause == PauseOptions.SFXVOLUME.value:
                            sfx.update_volume(sfx.sfx_volume - 0.1)
                            SFXVolumeText = ExpFont.render("SFX Volume: %.2f" % sfx.sfx_volume, False, (255, 255, 255))

                    if e.key == pygame.K_RIGHT:
                        if select_pause == PauseOptions.MUSICVOLUME.value:
                            current_volume = pygame.mixer.music.get_volume()
                            new_volume = max(0, min(1, current_volume + 0.1))
                            pygame.mixer.music.set_volume(new_volume)
                            MusicVolumeText = ExpFont.render("Music Volume: %.2f" % new_volume, False, (255, 255, 255))
                        if select_pause == PauseOptions.SFXVOLUME.value:
                            sfx.update_volume(sfx.sfx_volume + 0.1)
                            SFXVolumeText = ExpFont.render("SFX Volume: %.2f" % sfx.sfx_volume, False, (255, 255, 255))

                    if e.key == pygame.K_c or e.key == pygame.K_RETURN:
                        if select_pause == PauseOptions.CONTINUE.value:
                            isPause = False
                        elif select_pause == PauseOptions.RESTART.value:
                            running = False
                            switch_scene(scene)
                        elif select_pause == PauseOptions.QUIT.value:
                            running = False
                            switch_scene(scene_menu.scene)

        sc.fill((212, 105, 205))
        sc.blit(surf_game_zone, (WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 360))
        draw_group()
        sc.blit(player.image, player.rect)
        pygame.draw.rect(sc, (0, 0, 0), (WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 361, 565, 721), 3)

        sc.blit(ScoreTXT, ScoreTXT.get_rect(center=(195, 55)))
        LifeTXT = LifeFont.render("LIFE -> " + str(player.Life), False, (255, 150, 155))
        sc.blit(LifeTXT, LifeTXT.get_rect(center=(195, 125)))
        ExpTXT = ExpFont.render("EXP -> " + str(player.exp), False, (255, 255, 0))
        sc.blit(ExpTXT, ExpTXT.get_rect(center=(195, 255)))
        sc.blit(LOGO, LOGO.get_rect(center=(195, 595)))

        # Pause Method
        if not isPause:
            if player.Life <= 0:
                running = False
                switch_scene(scene_die.scene)
            player.update()
            update_group()
            collision(player)
            if timeEnd < 6055:
                create_enemy.stageCreateEnemy()
            if timeEnd >= 6650:
                if timeStartBoss == 0:
                    ImageFill.set_alpha(0)
                if timeStartBoss >= 250:
                    running = False
                    switch_scene(scene_boss.scene)
                alphaImg += 3
                ImageFill.set_alpha(alphaImg)
                timeStartBoss += 1
                sc.blit(ImageFill, ImageFill.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
            updateTXT()
            timeEnd += 1
        else:
            pause_texts = [BackTXT, RestartTXT, QuitTXT, MusicVolumeText, SFXVolumeText]
            YArrow = WINDOW_SIZE[1] // 2 - 50 + select_pause * 50

            sc.blit(ImageFill, ImageFill.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
            sc.blit(PauseTXT, PauseTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 125)))
            arrow_x_pos = WINDOW_SIZE[0] // 2 - pause_texts[select_pause].get_width() // 2 - 15
            sc.blit(ArrowImg, ArrowImg.get_rect(midright=(arrow_x_pos, YArrow - 1)))

            for i in range(len(pause_texts)):
                pause_texts[i].set_alpha(gray_option_alpha)
                if i == select_pause:
                    pause_texts[i].set_alpha(full_option_alpha)
                sc.blit(pause_texts[i],
                        pause_texts[i].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 50 + i * 50)))

        pygame.display.update()
        clock.tick(FPS)
