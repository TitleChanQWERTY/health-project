import pygame
from config import sc, clock, WINDOW_SIZE, switch_scene, createImage
from SCENE import scene_game
from font_config import ExpFont


def scene():
    running: bool = True
    FPS: int = 60

    Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/1.png")
    Logo_anim = pygame.transform.scale(Logo_anim, (535, 456))

    FrameAnim: int = 0

    select: int = 0

    RestartTXT = ExpFont.render("START", False, (255, 255, 255))
    QuitTXT = ExpFont.render("QUIT (please no)", False, (255, 255, 255))

    ArrowImg = createImage("ASSETS/SPRITE/UI/ARROW.png")
    ArrowImg = pygame.transform.scale(ArrowImg, (35, 25))
    YArrow = WINDOW_SIZE[1] // 2 - 50

    fontCreator = pygame.font.Font("ASSETS/FONT/joystix monospace.otf", 15)
    txtCreator = fontCreator.render("Creators: TitleChanQWERTY(F BOY), kemgoblin(Simple Coder), Maksym(Music Man Music!)",
                                    False,
                                    (255, 255, 255))

    pygame.mixer.music.load("ASSETS/OST/mainmenu.mp3")
    pygame.mixer.music.play()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN and FrameAnim >= 95:
                if e.key == pygame.K_DOWN:
                    if select == 0:
                        select = 1
                    elif select == 1:
                        select = 0
                if e.key == pygame.K_UP:
                    if select == 0:
                        select = 1
                    elif select == 1:
                        select = 0
                if e.key == pygame.K_c or e.key == pygame.K_RETURN:
                    if select == 0:
                        running = False
                        switch_scene(scene_game.scene)
                    elif select == 1:
                        running = False
                        switch_scene(None)

        sc.fill((185, 53, 196))
        if FrameAnim == 20:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/2.png")
        elif FrameAnim == 30:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/3.png")
        elif FrameAnim == 40:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/4.png")
        elif FrameAnim == 50:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/5.png")
        elif FrameAnim == 60:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/6.png")
        elif FrameAnim == 80:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/7.png")
        elif FrameAnim == 95:
            FrameAnim += 1
            Logo_anim = createImage("ASSETS/SPRITE/UI/LOGO_ANIM/8.png")
        elif FrameAnim >= 125:
            sc.blit(txtCreator, txtCreator.get_rect(center=(WINDOW_SIZE[0]//2, 690)))
            sc.blit(ArrowImg, ArrowImg.get_rect(center=(WINDOW_SIZE[0] // 2 - 119, YArrow - 1)))
            sc.blit(RestartTXT, RestartTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 50)))
            sc.blit(QuitTXT, QuitTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
            if select == 0:
                RestartTXT.set_alpha(300)
                QuitTXT.set_alpha(30)
                YArrow = WINDOW_SIZE[1] // 2 - 50
            elif select == 1:
                RestartTXT.set_alpha(30)
                QuitTXT.set_alpha(300)
                YArrow = WINDOW_SIZE[1] // 2
        else:
            FrameAnim += 1
        Logo_anim = pygame.transform.scale(Logo_anim, (535, 456))
        sc.blit(Logo_anim, Logo_anim.get_rect(center=(WINDOW_SIZE[0] // 2, 85)))
        pygame.display.update()
        clock.tick(FPS)
