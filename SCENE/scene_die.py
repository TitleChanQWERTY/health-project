import pygame
from config import switch_scene, WINDOW_SIZE, sc, clock, createImage
from font_config import ExpFont, LifeFont
from SCENE import scene_game, scene_menu


def scene():
    running: bool = True
    FPS: int = 60

    artDie = createImage("ASSETS/SPRITE/UI/DIE_SCREEN/1.png")
    artDie = pygame.transform.scale(artDie, (326, 326))
    artDie.set_alpha(0)

    alpha_die: int = 0

    FrameDie: int = 0

    bip_sound = pygame.mixer.Sound("ASSETS/SFX/hana.mp3")
    bip_sound.play()

    pygame.mixer.music.load("ASSETS/OST/You_died_.mp3")
    pygame.mixer.music.play()

    RestartTXT = ExpFont.render("RESTART", False, (255, 255, 255))
    QuitTXT = ExpFont.render("BACK TO MENU", False, (255, 255, 255))

    ArrowImg = createImage("ASSETS/SPRITE/UI/ARROW.png")
    ArrowImg = pygame.transform.scale(ArrowImg, (35, 25))
    YArrow = WINDOW_SIZE[1] // 2 - 50

    DieTXT = LifeFont.render("YOU f DIE", False, (255, 0, 0))
    DieTXT.set_alpha(0)

    select: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN and FrameDie >= 200:
                if e.key == pygame.K_DOWN:
                    if select == 0:
                        select = 1
                    elif select == 1:
                        select = 0
                if e.key == pygame.K_UP:
                    if select == 0:
                        select = 1
                    elif select == 1:
                        select = 0
                if e.key == pygame.K_c or e.key == pygame.K_RETURN:
                    if select == 0:
                        running = False
                        switch_scene(scene_game.scene)
                    elif select == 1:
                        running = False
                        switch_scene(scene_menu.scene)

        sc.fill((0, 0, 0))
        if FrameDie < 200:
            FrameDie += 1
            alpha_die += 2
            artDie.set_alpha(alpha_die)
            DieTXT.set_alpha(alpha_die)
        else:
            sc.blit(ArrowImg, ArrowImg.get_rect(center=(WINDOW_SIZE[0] // 2 - 119, YArrow - 1)))
            sc.blit(RestartTXT, RestartTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 50)))
            sc.blit(QuitTXT, QuitTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
        sc.blit(artDie, artDie.get_rect(center=(WINDOW_SIZE[0]//2, WINDOW_SIZE[1]//2+175)))
        sc.blit(DieTXT, DieTXT.get_rect(center=(WINDOW_SIZE[0]//2, 100)))
        if select == 0:
            RestartTXT.set_alpha(300)
            QuitTXT.set_alpha(30)
            YArrow = WINDOW_SIZE[1] // 2 - 50
        elif select == 1:
            RestartTXT.set_alpha(30)
            QuitTXT.set_alpha(300)
            YArrow = WINDOW_SIZE[1] // 2
        pygame.display.update()
        clock.tick(FPS)
