import pygame
from config import sc, clock, switch_scene, WINDOW_SIZE, createImage
from font_config import ScoreFont, ExpFont
from SCENE import scene_menu


def scene_dialog():
    running: bool = True
    FPS: int = 60

    txtDialog = ScoreFont.render("...", False, (255, 255, 255))

    imgEnd = createImage("ASSETS/SPRITE/UI/END_SCREEN.png")
    imgEnd = pygame.transform.scale(imgEnd, (298, 298))
    dialogue_lines = ["...", "Doctor,", "am I finally healthy?", "Yes.", "Now you can go home."]

    select: int = 0

    time_touch: int = 0
    wait_time: int = 50

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN and time_touch >= wait_time:
                select += 1

        sc.fill((0, 0, 0))
        if time_touch <= wait_time:
            time_touch += 1

        if select < len(dialogue_lines):
            txtDialog = ScoreFont.render(dialogue_lines[select], False, (255, 255, 255))

        if select >= len(dialogue_lines):
            running = False
            switch_scene(scene_thanks)

        if select >= 3:
            sc.blit(imgEnd, imgEnd.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
        sc.blit(txtDialog, txtDialog.get_rect(center=(WINDOW_SIZE[0] // 2, 670)))
        pygame.display.update()
        clock.tick(FPS)


def scene_thanks():
    running: bool = True
    FPS: int = 60

    txtThanks = ScoreFont.render("THANKS FOR PLAYING!", False, (255, 255, 255))

    DeveloperTXT = ExpFont.render("Developers:", False, (255, 255, 255))
    developers = ["TitleChanQWERTY", "kemgoblin", "Maksym"]
    developer_texts = []
    for dev in developers:
        text_surface = ExpFont.render(dev, False, (255, 255, 255))
        text_surface.set_alpha(0)
        developer_texts.append(text_surface)

    Frame: int = 0

    txtThanks.set_alpha(0)
    DeveloperTXT.set_alpha(0)

    AlphaTXT: int = 0

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN and Frame >= 90:
                running = False
                switch_scene(scene_menu.scene)

        sc.fill((0, 0, 0))
        if Frame < 200:
            Frame += 1
            AlphaTXT += 1
            txtThanks.set_alpha(AlphaTXT)
            DeveloperTXT.set_alpha(AlphaTXT)
            for d in developer_texts:
                d.set_alpha(AlphaTXT)

        sc.blit(txtThanks, txtThanks.get_rect(center=(WINDOW_SIZE[0] // 2, 95)))
        sc.blit(DeveloperTXT, DeveloperTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
        for i in range(len(developer_texts)):
            sc.blit(developer_texts[i],
                    developer_texts[i].get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 50 + i * 40)))
        pygame.display.update()
        clock.tick(FPS)
