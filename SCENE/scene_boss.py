import pygame
from config import sc, clock, WINDOW_SIZE, switch_scene, surf_game_zone, createImage
from SCENE import scene_die, scene_menu, scene_game, scene_end_title
from ENEMY import boss_f
from font_config import ScoreFont, LifeFont, ExpFont
from collision import collision
from screenshot import take
import group_config

SCORE: int = 0

ScoreTXT = ScoreFont.render("SCORE -> " + str(SCORE), False, (255, 255, 255))


def updateTXT():
    global ScoreTXT
    ScoreTXT = ScoreFont.render("SCORE -> " + str(SCORE), False, (255, 255, 255))


def scene():
    running: bool = True
    FPS: int = 60

    LOGO = createImage("ASSETS/SPRITE/UI/LOGO.png")
    LOGO = pygame.transform.scale(LOGO, (200, 200))

    isPause: bool = False

    PauseTXT = ScoreFont.render("YEA PAUSE", False, (255, 255, 255))

    BackTXT = ExpFont.render("BACK", False, (255, 255, 255))
    RestartTXT = ExpFont.render("RESTART", False, (255, 255, 255))
    QuitTXT = ExpFont.render("QUIT", False, (255, 255, 255))

    ArrowImg = createImage("ASSETS/SPRITE/UI/ARROW.png")
    ArrowImg = pygame.transform.scale(ArrowImg, (35, 25))
    YArrow = WINDOW_SIZE[1] // 2 - 150

    ImageFill = createImage("ASSETS/SPRITE/UI/FILL_IMAGE/1.png")
    ImageFill = pygame.transform.scale(ImageFill, WINDOW_SIZE)
    ImageFill.set_alpha(175)

    select_pause: int = 0

    pygame.mixer.music.load("ASSETS/OST/Le_boss.mp3")
    pygame.mixer.music.play(-1)

    boss_die_sfx = pygame.mixer.Sound("ASSETS/SFX/boss_die.wav")

    boss_f.Boss()

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                switch_scene(None)
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F6:
                    take()
                if e.key == pygame.K_ESCAPE:
                    if isPause:
                        isPause = False
                    else:
                        isPause = True
                if isPause:
                    if e.key == pygame.K_DOWN:
                        select_pause += 1
                        select_pause %= 3

                    if e.key == pygame.K_UP:
                        select_pause -= 1
                        select_pause %= 3

                    if e.key == pygame.K_c or e.key == pygame.K_RETURN:
                        if select_pause == 0:
                            isPause = False
                        elif select_pause == 1:
                            running = False
                            switch_scene(scene)
                        elif select_pause == 2:
                            running = False
                            switch_scene(scene_menu.scene)

        sc.fill((212, 105, 205))
        sc.blit(surf_game_zone, (WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 360))
        scene_game.draw_group()
        sc.blit(scene_game.player.image, scene_game.player.rect)
        pygame.draw.rect(sc, (0, 0, 0), (WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 361, 565, 721), 3)

        sc.blit(ScoreTXT, ScoreTXT.get_rect(center=(195, 55)))
        LifeTXT = LifeFont.render("LIFE -> " + str(scene_game.player.Life), False, (255, 150, 155))
        sc.blit(LifeTXT, LifeTXT.get_rect(center=(195, 125)))
        ExpTXT = ExpFont.render("EXP -> " + str(scene_game.player.exp), False, (255, 255, 0))
        sc.blit(ExpTXT, ExpTXT.get_rect(center=(195, 255)))
        sc.blit(LOGO, LOGO.get_rect(center=(195, 595)))

        # Pause Method
        if not isPause:
            if scene_game.player.Life <= 0:
                running = False
                switch_scene(scene_die.scene)
            for e in group_config.enemy:
                if e.Health <= 0:
                    pygame.mixer.music.stop()
                    boss_die_sfx.play()
                    running = False
                    switch_scene(scene_end_title.scene_dialog)
            scene_game.player.update()
            scene_game.update_group()
            collision(scene_game.player)
            updateTXT()
        else:
            if select_pause == 0:
                BackTXT.set_alpha(300)
                RestartTXT.set_alpha(30)
                QuitTXT.set_alpha(30)
                YArrow = WINDOW_SIZE[1] // 2 - 50
            elif select_pause == 1:
                BackTXT.set_alpha(30)
                RestartTXT.set_alpha(300)
                QuitTXT.set_alpha(30)
                YArrow = WINDOW_SIZE[1] // 2
            elif select_pause == 2:
                BackTXT.set_alpha(30)
                RestartTXT.set_alpha(30)
                QuitTXT.set_alpha(300)
                YArrow = WINDOW_SIZE[1] // 2 + 50
            sc.blit(ImageFill, ImageFill.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
            sc.blit(PauseTXT, PauseTXT.get_rect(center=(WINDOW_SIZE[0] // 2, 125)))
            sc.blit(ArrowImg, ArrowImg.get_rect(center=(WINDOW_SIZE[0] // 2 - 75, YArrow - 1)))
            sc.blit(BackTXT, BackTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 - 50)))
            sc.blit(RestartTXT, RestartTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2)))
            sc.blit(QuitTXT, QuitTXT.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2 + 50)))

        pygame.display.update()
        clock.tick(FPS)
